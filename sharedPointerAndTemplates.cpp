#include <iostream>
#include <memory>

template<typename gridtype>
void justSomeFunction(std::shared_ptr<gridtype> p) {
	p->get(); 
	std::cout << "reference count" << p.use_count() << "\n"; 
}

class gridtype {
public :
	explicit gridtype() {
		std::cout << "here" << std::endl ;
		a = 2; 
	}
	void get() {
		std::cout << " get here" << "\n"; 
	}

private :
	int a; 
};

int main() {
	gridtype g; 
	std::shared_ptr<gridtype> ptrgrid(new gridtype);
	justSomeFunction(ptrgrid); 
	
	getchar();
}